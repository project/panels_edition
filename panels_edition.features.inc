<?php

/**
 * Implementation of hook_ctools_plugin_api().
 */
function panels_edition_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "panelizer" && $api == "panelizer") {
    return array("version" => 1);
  }
  elseif ($module == "strongarm" && $api == "strongarm") {
    return array("version" => 1);
  }
}

/**
 * Implementation of hook_node_info().
 */
function panels_edition_node_info() {
  $items = array(
    'edition' => array(
      'name' => t('Edition'),
      'module' => 'features',
      'description' => t('An edition of the site, which includes a panels display and other metadata.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'has_body' => '1',
      'body_label' => t('Body'),
      'min_word_count' => '0',
      'help' => '',
    ),
  );
  return $items;
}

/**
 * Implementation of hook_views_api().
 */
function panels_edition_views_api() {
  return array(
    'api' => '3.0',
  );
}
