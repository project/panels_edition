<?php

/**
 * Implementation of hook_panelizer_defaults().
 */
function panels_edition_panelizer_defaults() {
  $export = array();

  $panelizer = new stdClass;
  $panelizer->disabled = FALSE; /* Edit this to true to make a default panelizer disabled initially */
  $panelizer->api_version = 1;
  $panelizer->name = 'node:edition:default';
  $panelizer->title = '';
  $panelizer->panelizer_type = 'node';
  $panelizer->panelizer_key = 'edition';
  $panelizer->no_blocks = FALSE;
  $panelizer->css_id = '';
  $panelizer->css = '';
  $panelizer->pipeline = 'standard';
  $panelizer->contexts = array();
  $panelizer->relationships = array();
  $panelizer->did = 852;
  $display = new panels_display;
  $display->layout = 'twocol';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'center' => NULL,
      'left' => NULL,
      'right' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '';
  $display->content = array();
  $display->panels = array();
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = '0';
  $panelizer->display = $display;
  $export['node:edition:default'] = $panelizer;

  return $export;
}
