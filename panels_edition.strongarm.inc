<?php

/**
 * Implementation of hook_strongarm().
 */
function panels_edition_strongarm() {
  $export = array();

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_options_edition';
  $strongarm->value = array(
    0 => 'revision',
  );
  $export['node_options_edition'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'upload_edition';
  $strongarm->value = '0';
  $export['upload_edition'] = $strongarm;

  return $export;
}
