<?php

/**
 * Implementation of hook_views_default_views().
 */
function panels_edition_views_default_views() {
  $views = array();

  // Exported view: panels_edition_front
  $view = new view;
  $view->name = 'panels_edition_front';
  $view->description = 'A view of the panels edition that should be used for the front page.';
  $view->tag = '';
  $view->base_table = 'node';
  $view->human_name = 'Front page panels edition';
  $view->core = 6;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Defaults */
  $handler = $view->new_display('default', 'Defaults', 'default');
  $handler->display->display_options['access']['type'] = 'none';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['row_plugin'] = 'panelizer_node_view';
  $handler->display->display_options['row_options']['links'] = 1;
  $handler->display->display_options['row_options']['comments'] = 0;
  $handler->display->display_options['row_options']['render_anything'] = 0;
  /* Relationship: Nodequeue: Queue (name) */
  $handler->display->display_options['relationships']['nodequeue_rel_queue_name']['id'] = 'nodequeue_rel_queue_name';
  $handler->display->display_options['relationships']['nodequeue_rel_queue_name']['table'] = 'node';
  $handler->display->display_options['relationships']['nodequeue_rel_queue_name']['field'] = 'nodequeue_rel_queue_name';
  $handler->display->display_options['relationships']['nodequeue_rel_queue_name']['required'] = 1;
  $handler->display->display_options['relationships']['nodequeue_rel_queue_name']['limit'] = 1;
  $handler->display->display_options['relationships']['nodequeue_rel_queue_name']['names'] = array(
    'panels_edition_front' => 'panels_edition_front',
  );

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page_1');
  $handler->display->display_options['path'] = 'panels-edition-front';
  $translatables['panels_edition_front'] = array(
    t('Defaults'),
    t('more'),
    t('Apply'),
    t('Reset'),
    t('Sort by'),
    t('Asc'),
    t('Desc'),
    t('Items per page'),
    t('- All -'),
    t('Offset'),
    t('queue'),
    t('Page'),
  );

  $views[$view->name] = $view;

  // Exported view: panels_edition_manager
  $view = new view;
  $view->name = 'panels_edition_manager';
  $view->description = 'Manage panels edition nodes on your site.';
  $view->tag = '';
  $view->base_table = 'node';
  $view->human_name = 'Panels edition manager';
  $view->core = 6;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Defaults */
  $handler = $view->new_display('default', 'Defaults', 'default');
  $handler->display->display_options['title'] = 'Panels editions';
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['access']['perm'] = 'edit any edition content';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '30';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['pager']['options']['id'] = '0';
  $handler->display->display_options['pager']['options']['expose']['items_per_page_options_all'] = 0;
  $handler->display->display_options['style_plugin'] = 'table';
  $handler->display->display_options['style_options']['columns'] = array(
    'title' => 'title',
    'status' => 'status',
    'created' => 'created',
    'name' => 'name',
    'clone_node' => 'clone_node',
    'panelizer_link' => 'panelizer_link',
    'panelizer_link_1' => 'panelizer_link_1',
    'edit_node' => 'edit_node',
  );
  $handler->display->display_options['style_options']['default'] = 'created';
  $handler->display->display_options['style_options']['info'] = array(
    'title' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
    ),
    'status' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
    ),
    'created' => array(
      'sortable' => 1,
      'default_sort_order' => 'desc',
      'align' => '',
      'separator' => '',
    ),
    'name' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
    ),
    'clone_node' => array(
      'align' => '',
      'separator' => '',
    ),
    'panelizer_link' => array(
      'align' => '',
      'separator' => '',
    ),
    'panelizer_link_1' => array(
      'align' => '',
      'separator' => '',
    ),
    'edit_node' => array(
      'align' => '',
      'separator' => '',
    ),
  );
  $handler->display->display_options['style_options']['override'] = 1;
  $handler->display->display_options['style_options']['sticky'] = 0;
  $handler->display->display_options['style_options']['empty_table'] = 0;
  /* Field: Node: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['title']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['title']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['title']['alter']['external'] = 0;
  $handler->display->display_options['fields']['title']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['title']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['title']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['title']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['title']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['title']['alter']['html'] = 0;
  $handler->display->display_options['fields']['title']['element_label_colon'] = 1;
  $handler->display->display_options['fields']['title']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['title']['hide_empty'] = 0;
  $handler->display->display_options['fields']['title']['empty_zero'] = 0;
  $handler->display->display_options['fields']['title']['link_to_node'] = 1;
  /* Field: Node: Published */
  $handler->display->display_options['fields']['status']['id'] = 'status';
  $handler->display->display_options['fields']['status']['table'] = 'node';
  $handler->display->display_options['fields']['status']['field'] = 'status';
  $handler->display->display_options['fields']['status']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['status']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['status']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['status']['alter']['external'] = 0;
  $handler->display->display_options['fields']['status']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['status']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['status']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['status']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['status']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['status']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['status']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['status']['alter']['html'] = 0;
  $handler->display->display_options['fields']['status']['element_label_colon'] = 1;
  $handler->display->display_options['fields']['status']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['status']['hide_empty'] = 0;
  $handler->display->display_options['fields']['status']['empty_zero'] = 0;
  $handler->display->display_options['fields']['status']['not'] = 0;
  /* Field: Node: Post date */
  $handler->display->display_options['fields']['created']['id'] = 'created';
  $handler->display->display_options['fields']['created']['table'] = 'node';
  $handler->display->display_options['fields']['created']['field'] = 'created';
  $handler->display->display_options['fields']['created']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['created']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['created']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['created']['alter']['external'] = 0;
  $handler->display->display_options['fields']['created']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['created']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['created']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['created']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['created']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['created']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['created']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['created']['alter']['html'] = 0;
  $handler->display->display_options['fields']['created']['element_label_colon'] = 1;
  $handler->display->display_options['fields']['created']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['created']['hide_empty'] = 0;
  $handler->display->display_options['fields']['created']['empty_zero'] = 0;
  /* Field: User: Name */
  $handler->display->display_options['fields']['name']['id'] = 'name';
  $handler->display->display_options['fields']['name']['table'] = 'users';
  $handler->display->display_options['fields']['name']['field'] = 'name';
  $handler->display->display_options['fields']['name']['label'] = 'Author';
  $handler->display->display_options['fields']['name']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['name']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['name']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['name']['alter']['external'] = 0;
  $handler->display->display_options['fields']['name']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['name']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['name']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['name']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['name']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['name']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['name']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['name']['alter']['html'] = 0;
  $handler->display->display_options['fields']['name']['element_label_colon'] = 1;
  $handler->display->display_options['fields']['name']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['name']['hide_empty'] = 0;
  $handler->display->display_options['fields']['name']['empty_zero'] = 0;
  $handler->display->display_options['fields']['name']['link_to_user'] = 1;
  $handler->display->display_options['fields']['name']['overwrite_anonymous'] = 0;
  /* Field: Node: Clone link */
  $handler->display->display_options['fields']['clone_node']['id'] = 'clone_node';
  $handler->display->display_options['fields']['clone_node']['table'] = 'node';
  $handler->display->display_options['fields']['clone_node']['field'] = 'clone_node';
  $handler->display->display_options['fields']['clone_node']['label'] = 'Clone';
  $handler->display->display_options['fields']['clone_node']['exclude'] = TRUE;
  $handler->display->display_options['fields']['clone_node']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['clone_node']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['clone_node']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['clone_node']['alter']['external'] = 0;
  $handler->display->display_options['fields']['clone_node']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['clone_node']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['clone_node']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['clone_node']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['clone_node']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['clone_node']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['clone_node']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['clone_node']['alter']['html'] = 0;
  $handler->display->display_options['fields']['clone_node']['element_label_type'] = '0';
  $handler->display->display_options['fields']['clone_node']['element_label_colon'] = 1;
  $handler->display->display_options['fields']['clone_node']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['clone_node']['hide_empty'] = 0;
  $handler->display->display_options['fields']['clone_node']['empty_zero'] = 0;
  $handler->display->display_options['fields']['clone_node']['text'] = 'Clone';
  /* Field: Node: Panelizer node link */
  $handler->display->display_options['fields']['panelizer_link']['id'] = 'panelizer_link';
  $handler->display->display_options['fields']['panelizer_link']['table'] = 'node';
  $handler->display->display_options['fields']['panelizer_link']['field'] = 'panelizer_link';
  $handler->display->display_options['fields']['panelizer_link']['exclude'] = TRUE;
  $handler->display->display_options['fields']['panelizer_link']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['panelizer_link']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['panelizer_link']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['panelizer_link']['alter']['external'] = 0;
  $handler->display->display_options['fields']['panelizer_link']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['panelizer_link']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['panelizer_link']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['panelizer_link']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['panelizer_link']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['panelizer_link']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['panelizer_link']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['panelizer_link']['alter']['html'] = 0;
  $handler->display->display_options['fields']['panelizer_link']['element_label_colon'] = 1;
  $handler->display->display_options['fields']['panelizer_link']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['panelizer_link']['hide_empty'] = 0;
  $handler->display->display_options['fields']['panelizer_link']['empty_zero'] = 0;
  $handler->display->display_options['fields']['panelizer_link']['panelizer_tab'] = 'panelizer/content';
  /* Field: Node: Panelizer node link */
  $handler->display->display_options['fields']['panelizer_link_1']['id'] = 'panelizer_link_1';
  $handler->display->display_options['fields']['panelizer_link_1']['table'] = 'node';
  $handler->display->display_options['fields']['panelizer_link_1']['field'] = 'panelizer_link';
  $handler->display->display_options['fields']['panelizer_link_1']['exclude'] = TRUE;
  $handler->display->display_options['fields']['panelizer_link_1']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['panelizer_link_1']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['panelizer_link_1']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['panelizer_link_1']['alter']['external'] = 0;
  $handler->display->display_options['fields']['panelizer_link_1']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['panelizer_link_1']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['panelizer_link_1']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['panelizer_link_1']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['panelizer_link_1']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['panelizer_link_1']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['panelizer_link_1']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['panelizer_link_1']['alter']['html'] = 0;
  $handler->display->display_options['fields']['panelizer_link_1']['element_label_colon'] = 1;
  $handler->display->display_options['fields']['panelizer_link_1']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['panelizer_link_1']['hide_empty'] = 0;
  $handler->display->display_options['fields']['panelizer_link_1']['empty_zero'] = 0;
  $handler->display->display_options['fields']['panelizer_link_1']['panelizer_tab'] = 'panelizer/layout';
  /* Field: Node: Edit link */
  $handler->display->display_options['fields']['edit_node']['id'] = 'edit_node';
  $handler->display->display_options['fields']['edit_node']['table'] = 'node';
  $handler->display->display_options['fields']['edit_node']['field'] = 'edit_node';
  $handler->display->display_options['fields']['edit_node']['label'] = 'Operations';
  $handler->display->display_options['fields']['edit_node']['alter']['alter_text'] = 1;
  $handler->display->display_options['fields']['edit_node']['alter']['text'] = '[edit_node] [clone_node] [panelizer_link] [panelizer_link_1]';
  $handler->display->display_options['fields']['edit_node']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['edit_node']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['edit_node']['alter']['external'] = 0;
  $handler->display->display_options['fields']['edit_node']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['edit_node']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['edit_node']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['edit_node']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['edit_node']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['edit_node']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['edit_node']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['edit_node']['alter']['html'] = 0;
  $handler->display->display_options['fields']['edit_node']['element_label_colon'] = 1;
  $handler->display->display_options['fields']['edit_node']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['edit_node']['hide_empty'] = 0;
  $handler->display->display_options['fields']['edit_node']['empty_zero'] = 0;
  $handler->display->display_options['fields']['edit_node']['text'] = 'Edit';
  /* Filter: Node: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'edition' => 'edition',
  );

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page_1');
  $handler->display->display_options['path'] = 'admin/content/panels-edition';
  $handler->display->display_options['menu']['type'] = 'normal';
  $handler->display->display_options['menu']['title'] = 'Panels editions';
  $handler->display->display_options['menu']['weight'] = '0';

  /* Display: Attachment */
  $handler = $view->new_display('attachment', 'Attachment', 'attachment_1');
  $handler->display->display_options['pager']['type'] = 'none';
  $handler->display->display_options['defaults']['header'] = FALSE;
  /* Header: Global: Text area */
  $handler->display->display_options['header']['area']['id'] = 'area';
  $handler->display->display_options['header']['area']['table'] = 'views';
  $handler->display->display_options['header']['area']['field'] = 'area';
  $handler->display->display_options['header']['area']['empty'] = FALSE;
  $handler->display->display_options['header']['area']['content'] = '<h2>Current front page edition</h2>';
  $handler->display->display_options['header']['area']['tokenize'] = 0;
  $handler->display->display_options['defaults']['relationships'] = FALSE;
  /* Relationship: Nodequeue: Queue (name) */
  $handler->display->display_options['relationships']['nodequeue_rel_queue_name']['id'] = 'nodequeue_rel_queue_name';
  $handler->display->display_options['relationships']['nodequeue_rel_queue_name']['table'] = 'node';
  $handler->display->display_options['relationships']['nodequeue_rel_queue_name']['field'] = 'nodequeue_rel_queue_name';
  $handler->display->display_options['relationships']['nodequeue_rel_queue_name']['required'] = 1;
  $handler->display->display_options['relationships']['nodequeue_rel_queue_name']['limit'] = 1;
  $handler->display->display_options['relationships']['nodequeue_rel_queue_name']['names'] = array(
    'panels_edition_front' => 'panels_edition_front',
  );
  $handler->display->display_options['defaults']['sorts'] = FALSE;
  $handler->display->display_options['attachment_position'] = 'after';
  $handler->display->display_options['displays'] = array(
    'page_1' => 'page_1',
    'default' => 0,
  );
  $translatables['panels_edition_manager'] = array(
    t('Defaults'),
    t('Panels editions'),
    t('more'),
    t('Apply'),
    t('Reset'),
    t('Sort by'),
    t('Asc'),
    t('Desc'),
    t('Items per page'),
    t('- All -'),
    t('Offset'),
    t('Title'),
    t('Published'),
    t('Post date'),
    t('Author'),
    t('Clone'),
    t('Panelizer node link'),
    t('Operations'),
    t('[edit_node] [clone_node] [panelizer_link] [panelizer_link_1]'),
    t('Edit'),
    t('Page'),
    t('Attachment'),
    t('Text area'),
    t('<h2>Current front page edition</h2>'),
    t('queue'),
  );

  $views[$view->name] = $view;

  return $views;
}
